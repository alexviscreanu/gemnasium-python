package pkgmngr

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
)

// PackageManager is a wrapper around a CLI tool for fetching & installing dependencies
type PackageManager interface {
	InstallDependencies(opts InstallOptions) error
	DependencyGraph() ([]byte, error)
	DepFile() string
}

func NewPackageManager(path string, cfg Config) (PackageManager, error) {
	if cfg.PipRequirementsFile != "" {
		_, file := filepath.Split(cfg.PipRequirementsFile)
		filePath := filepath.Join(path, file)
		if _, err := os.Stat(filePath); err != nil {
			return nil, fmt.Errorf("requirements file %s not found", filePath)
		}
		return pip{
			projectPath: path,
			depFile:     file,
		}, nil
	}
	for _, filename := range []string{"requirements.txt", "requirements.pip", "Pipfile", "requires.txt", "setup.py"} {
		if _, err := os.Stat(filepath.Join(path, filename)); err == nil {
			switch filename {
			case "Pipfile":
				return pipenv{projectPath: path}, nil
			case "requirements.txt", "requirements.pip", "requires.txt":
				return pip{
					projectPath: path,
					depFile:     filename,
				}, nil
			default:
				return setuptools{projectPath: path}, nil
			}
		}
	}
	return nil, fmt.Errorf("package manager not found for %s", path)
}

type Config struct {
	PipRequirementsFile string
}

// InstallOptions wraps a PackageManager's fetching and installation behaviors
type InstallOptions struct {
	CacheDir  string
	SkipFetch bool
	PipVersion string
}

func setupCmd(path string, cmd *exec.Cmd) *exec.Cmd {
	cmd.Dir = path
	cmd.Env = os.Environ()
	cmd.Stderr = os.Stderr
	return cmd
}