package pkgmngr

import (
	"fmt"
	"os"
	"os/exec"
)

type pip struct {
	projectPath string
	depFile     string
}

func (pm pip) DepFile() string {
	return pm.depFile
}

func (pm pip) InstallDependencies(opts InstallOptions) error {
	if opts.PipVersion != "" {
		if err := pm.installPip(opts.PipVersion); err != nil {
			return err
		}
	}

	// fetch dependencies unless dependencyPath is already set
	if !opts.SkipFetch {
		cmd := exec.Command("pip", "download", "--disable-pip-version-check", "--dest", opts.CacheDir, "-r", pm.DepFile())
		cmd.Stdout = os.Stdout
		if err := setupCmd(pm.projectPath, cmd).Run(); err != nil {
			return err
		}
	}

	args := []string{
		"install",
		"--disable-pip-version-check",
		"--find-links", opts.CacheDir,
		"--requirement", pm.DepFile(),
	}

	// When dependencies are passed explicitly, rely entirely off `find-links` path
	if opts.SkipFetch {
		args = append(args, "--no-index")
	}

	cmd := exec.Command("pip", args...)
	cmd.Stdout = os.Stdout

	return setupCmd(pm.projectPath, cmd).Run()
}

func (pm pip) DependencyGraph() ([]byte, error) {
	cmd := exec.Command("pipdeptree", "--json")

	return setupCmd(pm.projectPath, cmd).Output()
}

func (pm pip) installPip(version string) error {
	versionString := fmt.Sprintf(`pip==%s`, version)
	cmd := exec.Command("python", "/get-pip.py", "--disable-pip-version-check", "--no-cache-dir", versionString)
	cmd.Env = os.Environ()
	cmd.Stderr = os.Stderr
	return cmd.Run()
}