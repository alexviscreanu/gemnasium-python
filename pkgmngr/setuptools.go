package pkgmngr

import (
	"os"
	"os/exec"
)

type setuptools struct {
	projectPath string
}

func (pm setuptools) DepFile() string {
	return "setup.py"
}

func (pm setuptools) InstallDependencies(opts InstallOptions) error {
	cmd := exec.Command("/usr/local/bin/python", "setup.py", "install")
	cmd.Stdout = os.Stdout

	return setupCmd(pm.projectPath, cmd).Run()
}

func (pm setuptools) DependencyGraph() ([]byte, error) {
	cmd := exec.Command("pipdeptree", "--json")

	return setupCmd(pm.projectPath, cmd).Output()
}