package pkgmngr

import (
	"os"
	"os/exec"
)

type pipenv struct {
	projectPath string
}

func (pm pipenv) DepFile() string {
	return "Pipfile"
}

func (pm pipenv) InstallDependencies(opts InstallOptions) error {
	// TODO: We'd better install the required python version instead of forcing
	// to use the one available in this container.
	args := []string{
		"install",
		"--python", "/usr/local/bin/python",
	}

	cmd := exec.Command("pipenv", args...)
	cmd.Stdout = os.Stdout

	return setupCmd(pm.projectPath, cmd).Run()
}

func (pm pipenv) DependencyGraph() ([]byte, error) {
	cmd := exec.Command("pipenv", "graph", "--json")

	return setupCmd(pm.projectPath, cmd).Output()
}