package plugin

import (
	"os"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

const envPipRequirementsFile = "PIP_REQUIREMENTS_FILE"

func Match(path string, info os.FileInfo) (bool, error) {
	pipRequirementsFilePath := os.Getenv(envPipRequirementsFile)
	if pipRequirementsFilePath != "" {
		if strings.HasSuffix(path, pipRequirementsFilePath) {
			return true, nil
		}
		return false, nil
	}
	switch info.Name() {
	case "requirements.txt", "requirements.pip", "Pipfile", "requires.txt", "setup.py":
		return true, nil
	default:
		return false, nil
	}
}

func init() {
	plugin.Register("gemnasium-python", Match)
}
