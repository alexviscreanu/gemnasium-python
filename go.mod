module gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python/v2

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/urfave/cli v1.22.1
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.5.2
	gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2 v2.3.0
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
