package main

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"path/filepath"
	"strings"

	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python/v2/pkgmngr"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser/pipdeptree"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange"
)

const (
	flagDependencyPath = "pip-dependency-path"
	// PIP_DEPENDENCY_PATH is deprecated since GitLab 12.2 and should be removed in GitLab 13.0
	flagEnvDependencyPath = "DS_PIP_DEPENDENCY_PATH,PIP_DEPENDENCY_PATH"

	flagVrangeDir       = "vrange-dir"
	flagVrangePythonCmd = "vrange-python-cmd"

	flagPipVersion   = "pip-version"

	flagPipRequirementsFile = "pip-requirements-file"
)

func analyzeFlags() []cli.Flag {
	return append(
		scanner.Flags(),
		cli.StringFlag{
			Name:   flagVrangeDir,
			Usage:  "Path of the vrange directory",
			EnvVar: "VRANGE_DIR",
			Value:  "vrange",
		},
		cli.StringFlag{
			Name:   flagVrangePythonCmd,
			Usage:  "vrange command for Python",
			EnvVar: "VRANGE_PYTHON_CMD",
			Value:  "python/rangecheck.py",
		},
		cli.StringFlag{
			Name:   flagPipVersion,
			Usage:  "Pip version to install and use",
			EnvVar: "DS_PIP_VERSION",
		},
		cli.StringFlag{
			Name:   flagPipRequirementsFile,
			Usage:  "Path of pip requirements file to scan",
			EnvVar: "PIP_REQUIREMENTS_FILE",
		},
		cli.StringFlag{
			Name:   flagDependencyPath,
			Usage:  "Path to directory with pip dependencies",
			EnvVar: flagEnvDependencyPath,
		},
	)
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	// dependency scanner
	scan, err := scanner.NewScanner(c)
	if err != nil {
		return nil, err
	}

	// register version range resolvers
	if err := registerResolver(c); err != nil {
		return nil, err
	}

	opts := pkgmngr.InstallOptions{
		CacheDir:  "./dist",
		SkipFetch: false,
		PipVersion: c.String(flagPipVersion),
	}

	// If dependency path is passed explicitly, skip fetching and rely on cache
	if c.String(flagDependencyPath) != "" {
		opts.CacheDir = c.String(flagDependencyPath)
		opts.SkipFetch = true
	}

	cfg := pkgmngr.Config{
		PipRequirementsFile: c.String(flagPipRequirementsFile),
	}

	pm, err := pkgmngr.NewPackageManager(path, cfg)

	if err := pm.InstallDependencies(opts); err != nil {
		return nil, err
	}

	input, err := pm.DependencyGraph()
	if err != nil {
		return nil, err
	}

	// scan dependency graph
	buf := bytes.NewBuffer(input)
	depfile, err := scan.ScanReader(buf, "pipdeptree.json", pm.DepFile())
	if err != nil {
		return nil, err
	}
	result := []scanner.File{*depfile}

	// return affected sources
	var output bytes.Buffer
	enc := json.NewEncoder(&output)
	enc.SetEscapeHTML(false)
	enc.SetIndent("", "  ")
	if err := enc.Encode(result); err != nil {
		return nil, err
	}

	return ioutil.NopCloser(&output), nil
}

func registerResolver(c *cli.Context) error {
	cmd := strings.SplitN(c.String(flagVrangePythonCmd), " ", 2)
	path := filepath.Join(c.String(flagVrangeDir), cmd[0])
	args := cmd[1:]
	return vrange.RegisterCmd("python", path, args...)
}