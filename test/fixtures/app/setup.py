#!/usr/bin/env python

# Copyright (c) 2009 Google Inc. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from setuptools import find_packages
from setuptools import setup

setup(
  name='gyp',
  version='0.1',
  description='Some affected project',
  author='GitLab',
  author_email='gl-secure@gitlab.com',
  url='https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python',
  packages=find_packages(),
  install_requires=['cryptography==2.2.2',
                      'Django==1.11.3']
)
