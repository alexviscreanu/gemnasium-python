FROM python:3.6-alpine

ARG PIPENV_VERSION==2018.11.26

ENV VRANGE_DIR="/vrange"
ARG GEMNASIUM_VRANGE_BRANCH="v2.3.0"
ARG GEMNASIUM_VRANGE_REPO="https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium"
ARG GEMNASIUM_VRANGE_URL="$GEMNASIUM_VRANGE_REPO/raw/$GEMNASIUM_VRANGE_BRANCH/vrange"

ARG GEMNASIUM_DB_LOCAL_PATH="/gemnasium-db"
ARG GEMNASIUM_DB_REMOTE_URL="https://gitlab.com/gitlab-org/security-products/gemnasium-db.git"
ARG GEMNASIUM_DB_REF_NAME="master"

ENV GEMNASIUM_DB_LOCAL_PATH $GEMNASIUM_DB_LOCAL_PATH
ENV GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_REMOTE_URL
ENV GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REF_NAME

ADD get-pip.py /

RUN \
	# include gcc build env
	apk add --no-cache git build-base libffi-dev musl-dev openssl-dev python3-dev && \
	\
	# install pipdeptree, pipenv
	pip install pipdeptree pipenv==$PIPENV_VERSION && \
	\
	# install vrange/python
	mkdir -p $VRANGE_DIR/python && \
	wget -O $VRANGE_DIR/python/rangecheck.py $GEMNASIUM_VRANGE_URL/python/rangecheck.py && \
	wget -O $VRANGE_DIR/python/requirements.txt $GEMNASIUM_VRANGE_URL/python/requirements.txt && \
	chmod +x $VRANGE_DIR/python/rangecheck.py && \
	pip install -r $VRANGE_DIR/python/requirements.txt && \
	\
	# clone gemnasium-db
	git clone --branch $GEMNASIUM_DB_REF_NAME $GEMNASIUM_DB_REMOTE_URL $GEMNASIUM_DB_LOCAL_PATH

COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
